#This script reads on input a .cupt file, extracts 200 sentences, deletes VMWE annotations, keeps only the verb POS tags and writes the outcome in the parseme-tsv-pos format to stdout
#Parameters: none

BEGIN{
OFS = "\t"
nbsent=0
}
{
#print nbsent
if ($0 ~ /^$/) #Empty line signals a new sentence
	nbsent++

if ((NR != 1) && (nbsent < 200)) { #Omit the first line and go on if less than 100 sentences

	if ($0 ~ /(^#)|(^$)/) #Comment or empty line - recopy
		print $0
	else {
		if ($10 == "SpaceAfter=No")
			nsp = "nsp"
		else
			nsp = "_"

		#Print the POS if verbal
		if ($4 == "VERB")
			tag = $4
		else
			tag = "_"

		#Print the whole line
		#print "\'"$1, $2, nsp, "_", tag
		print "\'"$1, $2, nsp, "_", (tag==""?"":tag)
	}
}
}
