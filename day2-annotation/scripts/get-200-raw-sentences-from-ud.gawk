#This script reads on input a .conllu file, extracts 200 sentences, deletes VMWE annotations, keeps only the verb POS tags and writes the outcome in the parseme-tsv-pos format to stdout
#Parameters: none

BEGIN{
OFS = "\t"
MAX=200 #We want that many sentences
nbsent=0
}
{
#print nbsent
if ($0 ~ /^$/) #Empty line signals a new sentence
	nbsent++

if (nbsent < MAX) { #Go on if less than MAX sentences

	if ($0 ~ /(^#)|(^$)/) #Comment or empty line - recopy
		print $0
	else {
		if ($10 == "SpaceAfter=No")
			nsp = "nsp"
		else
			nsp = "_"

		#Print the POS if verbal
		if ($4 == "VERB")
			tag = $4
		else
			tag = "_"

		#Print the whole line
		#print "\'"$1, $2, nsp, "_", tag
		print "\'"$1, $2, nsp, "_", (tag==""?"":tag)
	}
}
}
