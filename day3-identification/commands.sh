########################################################################
# MWEs in a nutshell -- ESSLLI 2018 course
# List of commands used in day 3 - MWE identification
########################################################################

# This file can be executed as a bash script
# Therefore, all explanations are shown as bash comments

# 1. mwetoolkit installation
#---------------------------

# 1.1 - Install the requirements
# On Ubuntu-based Linux:
sudo apt-get install git python3 gcc python3-simplejson make wdiff libxml2 diffutils wget unzip
# Notice that we require python 3. The toolkit does not support python 2
# For other platforms, consult the website: 
# http://mwetoolkit.sourceforge.net/PHITE.php?sitesig=MWE&page=MWE_010_Install

# 1.2 - Download the toolkit from gitlab
git clone "https://gitlab.com/mwetoolkit/mwetoolkit3.git"

# 1.3 - Install the toolkit
cd mwetoolkit3
make
cd ..

# Note: if you have already installed the toolkit a while ago, you should
# nonetheless update the toolkit to obtain the latest version. 
# Therefore, please run the following commands:
cd mwetoolkit3
git pull
make
cd ..

# For convenience, we will use a bash variable pointing to the path 
# where the mwetoolkit was installed. Please set the path according to 
# the local folder in which you installed the toolkit:
export MWETK=`pwd`"/mwetoolkit3/"

# 2. Getting started
#-------------------
# All scripts in the mwetoolkit have the following usage template:
# ${MWETK}/bin/script.py -opt1 --opt2 argopt2 input.ext > output.ext
#  * script.py is a script in Python 3 that you can execute directly
#    (no need to call "python3 ${MWETK}/bin/script.py")
#  * input.ext is a mandatory input file with the "ext" extension. These
#    are often corpora, lexicons or patterns in XML, txt, cupt format
#  * Most scripts have short options (-opt1) and long options (--opt2).
#  * Some options have arguments themselves, such as "--opt2 argopt2"
#  * Most scripts will show the output on the terminal screen. To save
#    the results into a file, use bash redirection "> output.ext"

# All scripts have the "-h" option that shows the help
${MWETK}/bin/head.py -h

# Show the first lines of a file (by default 10 as in bash "head" tool)
${MWETK}/bin/head.py ${MWETK}/test/convert/local-input/corpus.cupt

# Example of a short option with an argument "-n 1" indicates that only
# one lines (instead of 10) should be shown
${MWETK}/bin/head.py -n 1 ${MWETK}/test/convert/local-input/corpus.cupt

# Example of a long option with an argument "--to HTML" indicates that 
# the output format should be HTML instead of the cupt format (by 
# default, the output format is the same as the input). Most scripts 
# have --from and --to options to control the filetypes of the input and
# outputs. The filetypes are described on the website:
# http://mwetoolkit.sourceforge.net/PHITE.php?sitesig=MWE&page=MWE_070_File_types
${MWETK}/bin/head.py --to HTML ${MWETK}/test/convert/local-input/corpus.cupt

# 3. Lexicon matching
#--------------------

# 3.1 - Download the course's GitLab repository
# If you are reading this file, this step is probably not necessary
# git clone https://gitlab.com/parseme/mwesinanutshell/

# 3.2 - Navigate to day3-identification > parsemest2018
cd parsemest2018

# 3.3 - Choose a language you understand and unzip its corpus
# Here, we will use English as an example but please feel free to use
# another language, if available why not your mother tongue?
unzip EN.zip

# 3.4 - Start exploring the MWEs annotated in the test corpus
# view.py is similar to bash's "less"
# Use the up and down arrows to navigate, '/' to search, 'q' to quit
# Annotated MWEs are shown in green
# It is easier for humans to visualise plain text in "PlainCorpus" but
# the script works with any kind of supported corpus format
${MWETK}/bin/view.py --to PlainCorpus EN/test.cupt

# 3.5 - Write a tiny lexicon of (verbal) MWEs in a txt file
# Here is an example, please create one in a plain text file for your 
# language or enrich the example if you are working on English. 
# The syntax of this file is the following:
#  * One MWE per line, 
#  * The components of the MWE must be lemmas
#  * Use underscore "_" as word separator, and not space
cat ../lexicons/tiny-EN.txt

# 3.6 - Project the lexicon on the blind test corpus (no MWE annotation):
${MWETK}/bin/annotate_mwe.py -c ../lexicons/tiny-EN.txt EN/test.blind.cupt |
${MWETK}/bin/view.py --to PlainCorpus

# 3.7 - Experiment with option -g <num> and check whether more MWEs are recognised
# Increasing the value of -g increases the chances of finding discontinuous MWEs
# but increases the chances of finding false positives (coincidental coocurrence)
${MWETK}/bin/annotate_mwe.py -g 2 -c ../lexicons/tiny-EN.txt EN/test.blind.cupt # |
#${MWETK}/bin/view.py --to PlainCorpus

# 3.8 - Extract a list of annotated MWEs in the training corpus:
${MWETK}/bin/extract_candidates.py EN/train.cupt > ../lexicons/train-EN.xml

# 3.9 - Project the resulting lexicon on the blind test corpus allowing gaps:
${MWETK}/bin/annotate_mwe.py -g 3 -c ../lexicons/train-EN.xml \
                           EN/test.blind.cupt > EN/test.auto.cupt

# 3.10 - Visualise the result:
${MWETK}/bin/view.py --to PlainCorpus EN/test.auto.cupt

# 3.11 - Evaluate the results using the PARSEME ST 2018 script:
# --pred is the prediction generated automatically by MWE identification
# --gold is the human annotated file with correct (gold) MWEs
# You can also specify --train to say what was your training corpus
# With --train, you will have more measures about unseen/variants
bin/evaluate.py --pred EN/test.auto.cupt --gold EN/test.cupt


# 4. Sequence model (CRF)
#------------------------

# 4.1 - Learn a simple CRF using default features from the trial corpus
$MWETK/bin/train_crf.py trial/EN_trial-train.cupt

# 4.2 - Now apply the model to annotate the test file
$MWETK/bin/annotate_crf.py trial/EN_trial-test_gold.cupt > trial/EN_trial-test_crf.cupt

# 4.3 - Use the shared task script to evaluate the result
bin/evaluate.py --pred trial/EN_trial-test_crf.cupt --gold trial/EN_trial-test_gold.cupt \
                --train trial/EN_trial-train.cupt
                
# The trial corpus is too small to learn a reliable model
# Use a real corpus of the PARSEME ST to learn and evaluate your CRF
# You can use -f to indicate another features file 
# You can include external lexicons with association measures using -a
# You can play with hyperparameters like L2 regularization weight -r













