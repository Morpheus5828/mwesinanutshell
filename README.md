README
------
This is the official page of the [ESSLLI 2018](http://esslli2018.folli.info/courses/) introductory course on entitled **Multiword Expressions in a Nutshell**, taught by:

  * [Carlos Ramisch](http://pageperso.lis-lab.fr/carlos.ramisch/)
  * [Agata Savary](http://www.info.univ-tours.fr/~savary/)
  * [Aline Villavicencio](http://www.inf.ufrgs.br/~avillavicencio/)

TODO list for the trainees
------
**Before coming** to the course in Sofia, please, make sure that:
  * You have filled in the course [registration form](https://docs.google.com/forms/d/1ej3evi6EwZettflwrPMWv6__ikHwuMVF6mBY6Riq_sk/edit) and indicated your mother tongue
  * You can log in to [FLAT](http://mwe.phil.hhu.de/) with the following parameters (in case of issues contact [Agata](http://www.info.univ-tours.fr/~savary/)):
    * login: **givenname.surname.lg** (where names are in lowercase, with no diacritics, and "lg" is the lowercase 2-letter code of your mother tongue)
    * password: **givenname-surname-ESSLI-2018** (one 'L' only!)
    * configuration: **ALL LANGUAGES 2018**
  * Your account on FLAT contains an English corpus and a corpus in your mother tongue
  * Your name appears in the [list of project groups](ESSLLI-MWEs-in-an-nutshell-project-groups.pdf) (in case of issues contact [Agata](http://www.info.univ-tours.fr/~savary/))
  * You have installed the [mwetoolkit](http://mwetoolkit.sf.net/) on your laptop (in case of issues, contact [Carlos](http://pageperso.lif.univ-mrs.fr/carlos.ramisch/))
    1. Ensure you have access to a command line shell/terminal (Linux, Mac OS, Cygwin on Windows)
    2. Depending on your platform, verify and install the prerequisites listed on the [installation page](http://mwetoolkit.sourceforge.net/PHITE.php?sitesig=MWE&page=MWE_010_Install) (step 1 on the installation page)
    3. Follow the installation instructions on this page, 2 (git clone) and 3 (make). Step 4 is not necessary.
  * You have read at least the essential readings listed below

**After the course**, please, help us enhance our teaching skills by filling in the [feedback form](https://goo.gl/forms/NcJm6giQC1BtQfGL2). Many thanks!
  

Recommended readings
------  

  * Essential readings:

    * Constant et al. (2017) [Multiword Expression Processing: A Survey](https://www.mitpressjournals.org/doi/pdf/10.1162/COLI_a_00302) - **section 1**
    * [PARSEME annotation guidelines 1.1](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/) - **sections 1-4** (to learn how to display multilingual examples, go to [section 3](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs), and click on the button with your language's code on the top of the page to toggle examples in this language, if any)
    * Cordeiro et al. (2016) [Predicting the Compositionality of Nominal Compounds: Giving Word Embeddings a Hard Time](http://www.aclweb.org/anthology/P16-1187) - full paper, in preparation for lecture 4

  * Complementary readings:

    * Baldwin and Kim (2010) [Multiword Expressions](https://people.eng.unimelb.edu.au/tbaldwin/pubs/handbook2009.pdf)
    * Manning and Schütze (1999) [Collocations](https://nlp.stanford.edu/fsnlp/promo/colloc.pdf)
    * Constant and Nivre (2016) [A Transition-Based System for Joint Lexical and Syntactic Analysis](http://www.aclweb.org/anthology/P16-1016)
    * Schneider et al. (2014) [A Corpus and Model Integrating Multiword Expressions and Supersenses](http://www.aclweb.org/anthology/N15-1177)


Course outline
---------------
The course will take place in the [2nd week of ESSLLI](http://esslli2018.folli.info/schedule-week-2/), on 13-17 August, at 11:00-12:30
  * **Day 1**: **Starting the ball rolling** with the theoretical foundations (Agata, Aline, Carlos)
    * [Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/slides/ESSLLI2018-MWEs-day1.pdf?inline=false)
  * **Day 2**: **Getting** your **hands dirty** with MWE annotation (Agata)
    * [Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/slides/ESSLLI2018-MWEs-day2.pdf?inline=false)
  * **Day 3**: **Putting** our **finger** on MWE token identification (Carlos)
    * [Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/slides/ESSLLI2018-MWEs-day3.pdf?inline=false) 
  * **Day 4**: **Making sense** of MWE semantics (Aline)
    * [Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/slides/ESSLLI2018-MWEs-day4.pdf?inline=false) 
    * [Compositionality annotation form](http://www.inf.ufrgs.br/mwe/compounds_en/)
  * **Day 5**: **Calling it a day** with group project presentations (trainees)
    * [Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/slides/ESSLLI2018-MWEs-day5.pdf?inline=false)
    * [Course feedback form](https://goo.gl/forms/NcJm6giQC1BtQfGL2)
    * Student project slides
      * [Upload your slides here](https://drive.google.com/drive/folders/1ACqGCuziPifrTT6WNDN1ODcW81MAvvEn?usp=sharing)
      * [Group 5 - Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/day5-student-projects/group-5-project-slides.pdf?inline=false)
      * [Group 4 - Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/day5-student-projects/group-4-project-slides.pdf?inline=false)
      * [Group 3 - Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/day5-student-projects/group-3-project-slides.pdf?inline=false)
      * [Group 2 - Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/day5-student-projects/group-2-project-slides.pdf?inline=false)
      * [Group 1 - Slides PDF](https://gitlab.com/parseme/mwesinanutshell/raw/master/day5-student-projects/group-1-project-slides.pdf?inline=false)


Useful links
------

  * [Student project groups](ESSLLI-MWEs-in-an-nutshell-project-groups.pdf)
  * [PARSEME multilingual annotation guidelines](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/) for verbal multiword expressions
  * [PARSEME corpora](https://gitlab.com/parseme/sharedtask-data/tree/master/1.1) annotated for vebal MWEs in 20 languages
  * [mwetoolkit](http://mwetoolkit.sf.net/)
  * [FLAT](http://mwe.phil.hhu.de) annotation platform and its [user's guide](https://docs.google.com/document/d/1bmO_H5rZZFZODvHIKf6VC0a_RCQe2H81yrd_EypCVyQ/edit?ts=594aaeb5#heading=h.9rmrlccmp5fa)
  * [PARSEME website](http://www.parseme.eu)


License
-------

All the material in this repository, including all course slides, is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)


Acknowledgments
------
This courses builds on seminal works and material also from other authors. Additionally to the bibliographical items cited above and in the slides, we notably rely on corpora in about 20 languages (mother tongues of the trainees). These corpora have been extracted from:
  * [Universal Dependencies](http://universaldependencies.org/) treebanks
  * [PARSEME corpus of verbal MWEs](https://gitlab.com/parseme/sharedtask-data/tree/master/1.1)

We are grateful for the creators and administrators of these resources for making them openly available.

